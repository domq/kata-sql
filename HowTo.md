# HelloDojo Marketing

Ce fichier permet de documenter (et logguer) les étapes que vous avez suivies
pour réaliser les demandes du [README.md](README.md).

## Mise en place
Voici les étapes que j'ai suivies pour installer MySQL et importer les tables
  * Installation des outils: `sudo apt install mysql-server mysql-client mysql-workbench`
  * Création de la DB: `CREATE SCHEMA kata-sql DEFAULT CHARACTER SET utf8mb4;`
  * Utilisation de la fonction d'import pour créer les 3 tables.
J'ai choisi d'utiliser [MySQL Workbench](https://www.mysql.com/products/workbench/) comme client de base de donnée.
J'ai généré le schéma avec [MySQL Workbench](https://www.mysql.com/products/workbench/):

![Mon MLD](schema.jpg "Mon MLD généré avec XXX")


## Informations à récolter

### Générales
1. La table `people` contient `410` personnes, ma requête est:  
   `SELECT COUNT(*) FROM people;`
1. La table `people` contient `11` doublons, ma requête est:  
   `SELECT COUNT(*) - COUNT(DISTINCT(email)) AS duplicateEmails FROM people;`
1. La table `people` est triée par nom de famille, ma requête est:  
  `SELECT * FROM people ORDER BY lastname;`
1. Je trouve les 5 premiers enregistrements de la table `people`, ma requête est:  
  `SELECT * FROM people LIMIT 5; `
1. Je trouve tous les enregistrements de la table `people` qui contiennent `ojo`, ma requête est:  
  `SELECT * FROM people WHERE firstname LIKE "%ojo%" OR lastname LIKE "%ojo%";``
1. Les 5 personnes plus agées sont `LIST`, ma requête est:  
  `SELECT * FROM people ORDER BY birthdate ASC LIMIT 5;`
1. Les 5 personnes plus jeunes sont `LIST`, ma requête est:  
  `SELECT * FROM people WHERE birthdate < now() ORDER BY birthdate DESC LIMIT 5;`
1. La moyenne d'age est 19.5, ma requête est:  
  `SELECT AVG(DATEDIFF(now(),birthdate)/365) FROM people;``
1. Le plus long prénom est `Clementine`, ma requête est:  
  `SELECT CHAR_LENGTH(firstname) AS lenprenom, firstname FROM people ORDER BY lenprenom DESC limit 5;`
1. Le plus long nom est `Christensen`, ma requête est:  
  `SELECT CHAR_LENGTH(lastname) AS lennom, lastname FROM people ORDER BY lennom DESC limit 5;`
1. La plus longue paire nom + prénom est `Pennington`, 'Cheyenne' ma requête est:  
  SELECT CHAR_LENGTH(lastname) + CHAR_LENGTH(firstname)  AS lennom, lastname, firstname FROM people ORDER BY lennom DESC limit 5

### Invitations
1. Pour lister tous le membres de plus de 18 ans:  
  `SELECT (DATEDIFF(now(),birthdate)/365), birthdate FROM people WHERE DATEDIFF(now(), birthdate)/365 > 18;`
  a. et de moins de 60 ans:  
     `SELECT (DATEDIFF(now(),birthdate)/365), birthdate FROM people WHERE DATEDIFF(now(), birthdate)/365 < 60 AND DATEDIFF(now(), birthdate)/365 > 18;`
  a. qui ont une addresse email valide:  
  `SELECT * FROM people WHERE email LIKE '%@%.% AND DATEDIFF(now(), birthdate)/365 < 60 AND DATEDIFF(now(), birthdate)/365 > 18';`
1. Pour ajoutez une colonne `age`:  
   `SELECT *,DATEDIFF(now(), birthdate)/365 AS age FROM people WHERE email LIKE '%@%.%' HAVING age < 60 AND age > 18;`
1. Pour générer une liste contenant `Prénom Nom <email@provider.com>;`:  
   ` SELECT adress FROM (SELECT DATEDIFF(now(), birthdate)/365 AS age, CONCAT(firstname, ' ', lastname, ' ',email) as adress FROM people WHERE email LIKE '%@%.%' HAVING age < 60 AND age > 18) result;`
1. Avec cette requête:  
     `SELECT COUNT(*) FROM  people WHERE email LIKE "%.ch";`  
   je peux estimer que `70` personnes habitent en Suisse.

### Countries
1. La requête qui permet d'obtenir la liste d'options
   sous la forme: `<option value="XXX">YYY</option>` est:  
   `SELECT CONCAT("<option value=""", iso3, """>", name_en, "</option>") FROM countries;`
   (si l'interface graphique est présentement en anglais, ou bien
   `SELECT CONCAT("<option value=""", iso3, """>", name_fr, "</option>") FROM countries;`
   en français)
1. Pour avoir la liste d'options en plusieurs langues, je procède de la manière suivante:  
   j'imagine un mécanisme qui permet de connaître la langue préférée de l'utilisateur
    - cookie
    - paramètre dans chaque URL
    - sous-arborescence pour toutes les pages
    - préférence persistante dans le navigateur lue en JavaScript
    - préférences de langue telles que paramétrées dans le navigateur
    (à choix)
    et j'applique, en fonction, l'une des règles précédentes

### Jointure
1. Avec cette requête:  
     `SELECT count(*) FROM people LEFT JOIN countries_people ON people.id = countries_people.idperson LEFT JOIN countries ON countries.id = countries_people.idcountry WHERE name_fr = "Suisse";`  
   je sais que `370` habitent en Suisse.
1. Avec cette requête:  
     `SELECT count(*) FROM people LEFT JOIN countries_people ON people.id = countries_people.idperson LEFT JOIN countries ON countries.id = countries_people.idcountry WHERE name_fr != "Suisse";`  
   je sais que `43` personnes n'habitent pas en Suisse.
1. Avec cette requête:  
     `SELECT people.id FROM people LEFT JOIN countries_people ON people.id = countries_people.idperson LEFT JOIN countries ON countries.id = countries_people.idcountry WHERE name_fr IN ("France", "Allemagne", "Italie", "Autriche", "Lischenchtein");`  
   je liste les membres habitants en France, Allemagne, Italie, Autriche et Lischenchtein.
1. Cette requête:  
     `SELECT idcountry, countries.*, count(*) FROM countries_people LEFT JOIN countries ON idcountry = countries.id GROUP BY idcountry;`  
   permet de compter combien il y a de personnes par pays.
1. Cette requête:  
     `SELECT XXX FROM XXX`  
   liste les pays qui ne possèdent pas de personnes.
1. En exécutant cette requête:  
     `SELECT XXX FROM XXX`  
   je sais que `NAME`, `NAME` et `NAME` sont liés à plusieurs pays.
1. En exécutant cette requête:  
     `SELECT XXX FROM XXX`  
   je sais que `NAME` est lié à un pays qui n'existe pas dans la base.
1. De la manière suivante:  
     `SQL`  
   nous pouvons afficher le pourcentages de personnes par pays.


### Procédures

1. Cette requête permet d'extraire `tld` de l'adresse email et de le lier à la
   table `countries`:  
    `SELECT XXX FROM XXX`  
1. Pour ajouter une chaine si la jointure ne retourne rien, j'ai procédé de la
   manière suivante:  
     `STRING`
1. Avec `STRING`, nous pouvons partager le mécanisme qui extrait le `tld`.

### Vue
1. J'ai créé une vue bien pratique contenant toutes les infomrations utiles à
   un humain. Ma requête est:  
   `CREATE XXX`

### Finances
1. J'ai créé une table pour les finances. Ma requête est:  
   `CREATE XXX`
1. J'ai modifié la vue en y ajoutant les finances. Ma requête est:  
   `UPDATE XXX`
